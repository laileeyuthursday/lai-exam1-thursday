#include <stdexcept>
#include "../inc/stack.h"

stack::stack() {
    stack_top = nullptr;

}

stack::~stack() {
    while(stack_top != nullptr);
    this->pop();
}

void stack::pop() {
    if(stack_top != nullptr && stack_top->next != nullptr) {
        stack_top = stack_top->next;
    }
}

void stack::push(struct value_date input) {
    stack_node *temp = new stack_node(input);
    if(stack_top != nullptr) {
        temp->next = stack_top;
        stack_top = temp;
        stack_top->next = temp->next;
    }else stack_top = temp;
}

const struct value_date stack::top() const {
    value_date temp;

    if (stack_top != nullptr){

    temp = stack_top->data;
        return temp;
}
    else throw;
}

bool stack::empty() {
    if(stack_top == nullptr) return true;
    else return false;
}

stack &stack::operator=(const stack &RHS) {
    if(this != &RHS){
        this->stack_top = RHS.stack_top;
    } else return *this;
    return *this;
}

stack stack::operator+(const stack &RHS) const {
    stack temp;
    stack original;
    stack input;
    original = *this;
    input = RHS;
        if (original.stack_top->data.date <= input.stack_top->data.date) {
            temp.push(original.stack_top->data);
            original.pop();
        } else {
            temp.push(input.stack_top->data);
            input.pop();
        }

    return temp;
}
